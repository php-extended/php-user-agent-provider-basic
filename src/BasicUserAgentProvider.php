<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-user-agent-provider-basic library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\UserAgent;

use ArrayIterator;
use Iterator;

/**
 * BasicUserAgentProvider class file.
 * 
 * This class represents a very basic user agent that provides the most recent
 * user agent for the windows platform with the chrome browser.
 * 
 * @author Anastaszor
 */
class BasicUserAgentProvider implements UserAgentProviderInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UserAgent\UserAgentProviderInterface::search()
	 */
	public function search(UserAgentQueryInterface $query, int $page = 1) : Iterator
	{
		$next = $this->getNextUserAgent();
		
		return new ArrayIterator(null === $next ? [] : [$next]);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UserAgent\UserAgentProviderInterface::getNextUserAgent()
	 */
	public function getNextUserAgent() : ?UserAgentInterface
	{
		return new UserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/133.0.0.0 Safari/537.36');
	}
	
}
