<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-user-agent-provider-basic library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\UserAgent\BasicUserAgentProvider;
use PhpExtended\UserAgent\UserAgent;
use PhpExtended\UserAgent\UserAgentQueryInterface;
use PHPUnit\Framework\TestCase;

/**
 * BasicUserAgentProviderTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\UserAgent\BasicUserAgentProvider
 *
 * @internal
 *
 * @small
 */
class BasicUserAgentProviderTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var BasicUserAgentProvider
	 */
	protected BasicUserAgentProvider $_object;
	
	/**
	 * The expected user agent string;.
	 * 
	 * @var string
	 */
	protected string $_expectedUA = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/133.0.0.0 Safari/537.36';
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testQuery() : void
	{
		$this->assertEquals(new ArrayIterator([new UserAgent($this->_expectedUA)]), $this->_object->search($this->createMock(UserAgentQueryInterface::class)));
	}
	
	public function testNext() : void
	{
		$this->assertEquals(new UserAgent($this->_expectedUA), $this->_object->getNextUserAgent());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new BasicUserAgentProvider();
	}
	
}
