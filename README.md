# php-extended/php-user-agent-provider-basic

A very basic user agent provider

![coverage](https://gitlab.com/php-extended/php-user-agent-provider-basic/badges/main/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-user-agent-provider-basic/badges/main/coverage.svg?style=flat-square)

This library is updated once a week, every sunday.


## Last Updated Date : 2025-03-09


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-user-agent-provider-basic ^8`


## Basic Usage

This library is to provide a standard and updated user agent that may be used in
headers to http queries.

```php

use PhpExtended\UserAgent\BasicUserAgentProvider;

$provider = new BasicUserAgentProvider();
$userAgent = $provider->getNextUserAgent();
echo $userAgent->__toString(); // Mozilla/5.0 ...

```


## License

MIT (See [license file](LICENSE)).