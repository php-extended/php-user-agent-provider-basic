#!/bin/bash
# This script is to update the class and test file and push the update
set -eu
IFS=$'\n\t'

CURRDIR=$(dirname "$0")
CURRDIR=$(realpath "$CURRDIR")

# https://gist.github.com/BR0kEN-/a84b18717f8c67ece6f7
# https://stackoverflow.com/questions/5031764/position-of-a-string-within-a-string-using-linux-shell-script

# @param string $1
#   Input string.
# @param string $2
#   String that will be searched in input string.
strpos()
{
	# delete from substring to the end
	x="${1%%$2*}"
	# the remaining length is the position
	[[ "$x" = "$1" ]] && echo -1 || echo "${#x}"
}

echo "[$(date '+%Y-%m-%d %H:%M:%S')]"
cd "$CURRDIR"
git pull --all
# TODO resolve from source once unblocked
#HTML=$(wget -O - "https://www.whatismybrowser.com/guides/the-latest-user-agent/chrome")

#UAGENT=$(php "$CURRDIR/getuseragentfromhtml.php" '$HTML")
echo "[$(date '+%Y-%m-%d %H:%M:%S')]"
UAGENT=$(wget -O - "https://jnrbsn.github.io/user-agents/user-agents.json" | jq ".[0]" | sed 's/"//g')

php "$CURRDIR/updateuseragent.php" "$UAGENT"

README=$(cat "$CURRDIR/README.md")
CURDATE=`date +%Y-%m-%d`
SED="s/Last Updated Date : [0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]/Last Updated Date : $CURDATE/g"
README=`echo "$README" | sed "$SED"`
printf "%s" "$README" > "$CURRDIR/README.md"

cd "$CURRDIR"
echo "[$(date '+%Y-%m-%d %H:%M:%S')]"
GITTAG=`git describe --abbrev=0 --tags`
GITTAG=`php -r "echo implode('.', [explode('.', '$GITTAG')[0], explode('.', '$GITTAG')[1], ((string) (1 + ((int) explode('.', '$GITTAG')[2])))]);"`
echo "[$(date '+%Y-%m-%d %H:%M:%S')]"
git add --all
echo "[$(date '+%Y-%m-%d %H:%M:%S')]"
git commit -m "Automatic update at $CURDATE"
echo "[$(date '+%Y-%m-%d %H:%M:%S')]"
git tag "$GITTAG"
echo "[$(date '+%Y-%m-%d %H:%M:%S')]"
git push --all ssh://git@gitlab.com/php-extended/php-user-agent-provider-basic.git
echo "[$(date '+%Y-%m-%d %H:%M:%S')]"
git push --tags ssh://git@gitlab.com/php-extended/php-user-agent-provider-basic.git
echo "END OF SCRIPT -- SUCCESS"
