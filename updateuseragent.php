<?php

global $argv;

if(!isset($argv[1]))
{
	throw new InvalidArgumentException("The first argument should be the user agent.");
}
// TODO uncomment when get data from source
// $html = $argv[1];

// $beginpos = strpos($html, '<span class="code">');
// if($beginpos === false)
// {
// 	throw new RuntimeException("Failed to find the span code from html text : "."\n\n\n".$html);
// }

// $striped = substr($html, $beginpos + 19);

// $endpos = strpos($striped, '</span>');
// if($endpos === false)
// {
// 	throw new RuntimeException("Failed to find the end span code from html text : "."\n\n\n".$striped);
// }

// $uagent = substr($html, 0, $endpos);
$uagent = (string) $argv[1];
if(strlen($uagent) > 1000 || strlen($uagent) < 40)
{
	// sanity check
	throw new RuntimeException("Expected user agent under 1000 chars, ".strlen($uagent)." found in \n\n\n".$uagent);
}

// {{{ Update BasicUserAgentProvider
$classPath = __DIR__.'/src/BasicUserAgentProvider.php';
$classData = file_get_contents($classPath);

$beginPos = strpos($classData, 'new UserAgent(');
if($beginPos === false)
{
	throw new RuntimeException('Failed to find instanciation of new user agent in BasicUserAgentProvider class');
}

$endPos = strpos($classData, ');', $beginPos + 14);
if($endPos === false)
{
	throw new RuntimeException('Failed to find end of instanciation in BasicUserAgentProvider class');
}

$newClassData = substr($classData, 0, $beginPos + 14)."'".strtr($uagent, ["'" => "\\'"])."'".substr($classData, $endPos);
$success = file_put_contents($classPath, $newClassData);
if($success === false)
{
	throw new RuntimeException('Failed to write new BasicUserAgentProvider class data');
}
// }}}


// {{{ Update BasicUserAgentProviderTest
$testPath = __DIR__.'/tests/BasicUserAgentProviderTest.php';
$testData = file_get_contents($testPath);

$beginPos = strpos($testData, "expectedUA = '");
if($beginPos === false)
{
	throw new RuntimeException('Failed to find expected user agent string un BasicUserAgentProviderTest class');
}

$endPos = strpos($testData, "';", $beginPos + 13);
if($endPos === false)
{
	throw new RuntimeException('Failed to find end of expected user agent stirng in BasicUserAgentProviderTest class');
}

$newTestData = substr($testData, 0, $beginPos + 14).strtr($uagent, ["'" => "\\'"]).substr($testData, $endPos);
$success = file_put_contents($testPath, $newTestData);
if($success === false)
{
	throw new RuntimeException('Failed to write new BasicUserAgentProviderTest class data');
}

// }}}

